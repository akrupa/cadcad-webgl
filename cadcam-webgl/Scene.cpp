//
//  Scene.cpp
//  cadcam-webgl
//
//  Created by Adrian Krupa on 12.04.2014.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#include <fstream>
#include "rapidjson/document.h"		// rapidjson's DOM-style API
#include "rapidjson/filestream.h"	// wrapper of C stream for prettywriter as output
#include "Scene.h"
#include "webgl-utils.h"
#include "gl-matrix-min.h"
#include "htmlFile.h"

using namespace std;

Scene::Scene() {

}

int Scene::parseSceneFromFile(char *filename) {
    
    Document document;
    
    FILE * pFile = fopen (filename , "r");
    if (!pFile) {
        printf("Error: cannot find file: %s\n", filename);
        return -1;
    }
    FileStream fs(pFile);
    
    if (document.ParseStream<0>(fs).HasParseError()) {
        printf("Error: cannot parse json file: %s\n", filename);
		return -1;
    }
    
    if (!document.HasMember("scene")) {
        printf("Error: cannot find scene entity\n");
		return -1;
    }
    
    Value& scene = document["scene"];
    return parseRootObject(scene);
}

int Scene::parseRootObject(Value &rootObject) {
    
    if (!rootObject.IsArray() && !rootObject.IsObject()) {
        printf("Error: root object is neither array nor object\n");
        return -1;
    }
    
    if (rootObject.IsArray()) {
        for (SizeType i = 0; i < rootObject.Size(); i++) {
            SceneObject obj;
            if(obj.parseObject(rootObject[i]) == 0) {
                objects.push_back(obj);
            }
        }
    } else if (rootObject.IsObject()) {
        SceneObject obj;
        if(obj.parseObject(rootObject) == 0) {
            objects.push_back(obj);
        }
    }
    
    return 0;
}

int Scene::loadObjects() {
    
    for (auto &object : objects) {
        if(object.loadObject() != 0)
            return -1;
    }
    
    return 0;
}

void Scene::generateHTML() {
    ofstream webglUtilsOutputFile("webgl-utils.js");
    webglUtilsOutputFile << webgl_utils;
    webglUtilsOutputFile.close();
    
    ofstream glmatrixminOutputFile("gl-matrix-min.js");
    glmatrixminOutputFile << gl_matrix_min;
    glmatrixminOutputFile << gl_matrix_min2;
    glmatrixminOutputFile << gl_matrix_min3;
    glmatrixminOutputFile << gl_matrix_min4;
    glmatrixminOutputFile << gl_matrix_min5;
    glmatrixminOutputFile.close();
    
    ofstream htmlOutputFile("index.html");
    htmlOutputFile << htmlFile;
    htmlOutputFile.close();
    
    ofstream modelsOutputFile("models.js");
    modelsOutputFile << "var vertices = [" << endl;
    
    for (int i=0; i<objects.size(); i++) {
        objects[i].writeVertices(&modelsOutputFile);
        if (i != objects.size()-1) {
            modelsOutputFile << "," << endl;
        }
    }
    
    modelsOutputFile << "];"<<endl<<endl;;
    
    modelsOutputFile << "var indices = [" << endl;
    int offset = 0;
    for (int i=0; i<objects.size(); i++) {
        offset = objects[i].writeIndices(&modelsOutputFile, offset);
        if (i != objects.size()-1) {
            modelsOutputFile << "," << endl;
        }
    }
    modelsOutputFile << "];"<<endl<<endl;;

    modelsOutputFile.close();
}

