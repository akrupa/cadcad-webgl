//
//  Scene.h
//  cadcam-webgl
//
//  Created by Adrian Krupa on 12.04.2014.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#ifndef __cadcam_webgl__Scene__
#define __cadcam_webgl__Scene__

#include <iostream>
#include <vector>
#include "SceneObject.h"

using namespace rapidjson;
using namespace std;

class Scene {
    
private:
    vector<SceneObject> objects;
    int parseRootObject(Value& rootObject);
    
public:
    Scene();
    int parseSceneFromFile(char *filename);
    int loadObjects();
    void generateHTML();
};

#endif /* defined(__cadcam_webgl__Scene__) */
