//
//  SceneObject.cpp
//  cadcam-webgl
//
//  Created by Adrian Krupa on 12.04.2014.
//  Copyright (c) 2014 AKBC. All rights reserved.
//
#define _USE_MATH_DEFINES
#include <fstream>
#include <cmath>
#include "SceneObject.h"

SceneObject::SceneObject() {
}

int SceneObject::parseObject(Value& obj) {
    if (!obj.IsObject()) {
        printf("Error parsing object - not an object.\n");
        return -1;
    }
    
    if (parseName(obj) != 0) {
        return -1;
    }
    if (parseObjectName(obj) != 0) {
        return -1;
    }
    if (parsePosition(obj) != 0) {
        return -1;
    }
    if (parseRotation(obj) != 0) {
        return -1;
    }
    if (parseScale(obj) != 0) {
        return -1;
    }
    if (parseColor(obj) != 0) {
        return -1;
    }
    if (parseChildren(obj) != 0) {
        return -1;
    }
    printf("---------------------------\n");
    printf("Parsed object:\nName: %s\nFilename: %s\nPosition:\t(%.2f, %.2f, %.2f)\nRotation:\t(%.2f, %.2f, %.2f)\nScale:\t\t(%.2f, %.2f, %.2f)\nColor:\t\t(%.3f, %.3f, %.3f)\nChildren count: %d\n", objectName.c_str(), objectFilename.c_str(), position[0], position[1], position[2], rotation[0], rotation[1], rotation[2], scale[0], scale[1], scale[2], color[0], color[1], color[2], (int)children.size());
    printf("---------------------------\n");

    return 0;
}

int SceneObject::parseName(Value& obj) {
    
    if (!obj.HasMember("name")) {
        printf("Error: object has no name member\n");
        return -1;
    }
    
    Value& name = obj["name"];
    if (!name.IsString()) {
        printf("Error object name is not a string\n");
        return -1;
    }
    const char *nameString = name.GetString();
    objectName.assign(nameString, strlen(nameString));
    
    return 0;
}

int SceneObject::parseObjectName(Value& obj) {
    
    if (!obj.HasMember("object")) {
        printf("Error: object has no object member\n");
        return -1;
    }
    
    Value& objectFilenameValue = obj["object"];
    if (!objectFilenameValue.IsString()) {
        printf("Error: object filename is not a string\n");
        return -1;
    }
    const char *objectFilenameString = objectFilenameValue.GetString();
    objectFilename.assign(objectFilenameString, strlen(objectFilenameString));
  
    return 0;
}

int SceneObject::parsePosition(Value& obj) {
    
    if (!obj.HasMember("position")) {
        printf("Error: object has no position member\n");
        return -1;
    }
    
    Value& objPosition = obj["position"];
    if (!objPosition.IsArray()) {
        printf("Error: object position is not an array\n");
        return -1;
    }
    
    if(objPosition.Size() != 3) {
        printf("Error: object position array has wrong size\n");
        return -1;
    }
    
    for (int i=0; i<3; i++) {
        if (!objPosition[i].IsNumber()) {
            printf("Error: object position array member (%d) is not number\n", i);
            return -1;
        }
        position[i] = objPosition[i].GetDouble();
    }
    
    return 0;
}

int SceneObject::parseRotation(Value& obj) {
    
    if (!obj.HasMember("rotation")) {
        printf("Error: object has no rotation member\n");
        return -1;
    }
    
    Value& objRotation = obj["rotation"];
    if (!objRotation.IsArray()) {
        printf("Error: object rotation is not an array\n");
        return -1;
    }
    
    if(objRotation.Size() != 3) {
        printf("Error: object rotation array has wrong size\n");
        return -1;
    }
    
    for (int i=0; i<3; i++) {
        if (!objRotation[i].IsNumber()) {
            printf("Error: object rotation array member (%d) is not number\n", i);
            return -1;
        }
        rotation[i] = objRotation[i].GetDouble();
    }
    
    return 0;
}

int SceneObject::parseScale(Value& obj) {
    
    if (!obj.HasMember("scale")) {
        printf("Error: object has no scale member\n");
        return -1;
    }
    
    Value& objScale = obj["scale"];
    if (!objScale.IsArray()) {
        printf("Error: object scale is not an array\n");
        return -1;
    }
    
    if(objScale.Size() != 3) {
        printf("Error: object scale array has wrong size\n");
        return -1;
    }
    
    for (int i=0; i<3; i++) {
        if (!objScale[i].IsNumber()) {
            printf("Error: object scale array member (%d) is not number\n", i);
            return -1;
        }
        scale[i] = objScale[i].GetDouble();
    }
    
    return 0;
}

int SceneObject::parseColor(Value &obj) {
    if (!obj.HasMember("color")) {
        color[0] = 0;
        color[1] = 0;
        color[2] = 0;
        return 0;
    }
    
    Value& objColor = obj["color"];
    if (!objColor.IsArray()) {
        printf("Error: object color is not an array\n");
        return -1;
    }
    
    if(objColor.Size() != 3) {
        printf("Error: object color array has wrong size\n");
        return -1;
    }
    
    for (int i=0; i<3; i++) {
        if (!objColor[i].IsNumber()) {
            printf("Error: object color array member (%d) is not number\n", i);
            return -1;
        }
        color[i] = objColor[i].GetDouble();
    }
    
    return 0;
}

int SceneObject::parseChildren(Value& obj) {
    
    if (obj.HasMember("children")) {
        
        Value& objChildren = obj["children"];
        
        if (!objChildren.IsArray()) {
            printf("Error: children object is not an array\n");
            return -1;
        }
        
        for (SizeType i = 0; i < objChildren.Size(); i++) {
            SceneObject sceneObj;
            if(sceneObj.parseObject(objChildren[i]) == 0) {
                children.push_back(sceneObj);
            }
        }
        
        
    }

    
    return 0;
}

int SceneObject::loadObject() {
    
	ifstream file(objectFilename);
    string temp;
    
    if (file.is_open()) {
        while (getline(file, temp)) {
            if (temp.length() > 2 && temp[1] == ' ') {
                if (temp[0] == 'v') {
                    array<float, 3> tempArray;
                    sscanf(temp.c_str(), "%*s %f %f %f\n", &tempArray[0], &tempArray[1], &tempArray[2]);
                    vertices.push_back(tempArray);
                } else if(temp[0] == 'f') {
                    array<int, 3> tempArray;
                    sscanf(temp.c_str(), "%*s %d %d %d\n", &tempArray[0], &tempArray[1], &tempArray[2]);
                    triangles.push_back(tempArray);
                }
            }
        }
    } else {
        return -1;
    }
    
    for (auto &child : children) {
        if(child.loadObject() != 0)
            return -1;
    }
    printf("Loaded object from file: %s\n", objectFilename.c_str());
    
    return 0;
}

void SceneObject::computeNormals() {
    vector<vector<array<float, 3>>> verticesNormals(vertices.size());
    
    bool computeBetterNormals = true;
    
    for (int i=0; i<triangles.size(); i++) {
        int v1 = triangles[i][0]-1;
        int v2 = triangles[i][1]-1;
        int v3 = triangles[i][2]-1;
        
        //wektory v i w prostopadłe do powierzchni trójkąta
        float vx = vertices[v2][0] - vertices[v1][0];
        float vy = vertices[v2][1] - vertices[v1][1];
        float vz = vertices[v2][2] - vertices[v1][2];
        
        float wx = vertices[v3][0] - vertices[v1][0];
        float wy = vertices[v3][1] - vertices[v1][1];
        float wz = vertices[v3][2] - vertices[v1][2];
        
        //cross product
        float nx = (vy*wz) - (vz*wy);
        float ny = (vz*wx) - (vx*wz);
        float nz = (vx*wy) - (vy*wx);
        
        //normalize
        if (abs(nx)+abs(ny)+abs(nz) != 0) {
            //float div = sqrt(nx*nx+ny*ny+nz*nz);
            //nx /= div;
            //ny /= div;
            //nz /= div;
            array<float, 3> tempArray;
            tempArray[0] = nx;
            tempArray[1] = ny;
            tempArray[2] = nz;
            if (computeBetterNormals) {
                for (int s = 0; s < vertices.size(); s++) {
                    if (vertices[s][0] == vertices[v1][0] &&
                        vertices[s][1] == vertices[v1][1] &&
                        vertices[s][2] == vertices[v1][2]) {
                        
                        verticesNormals[s].push_back(tempArray);
                    }
                }
                
                for (int s = 0; s < vertices.size(); s++) {
                    if (vertices[s][0] == vertices[v2][0] &&
                        vertices[s][1] == vertices[v2][1] &&
                        vertices[s][2] == vertices[v2][2]) {
                        
                        verticesNormals[s].push_back(tempArray);
                    }
                }
                
                for (int s = 0; s < vertices.size(); s++) {
                    if (vertices[s][0] == vertices[v3][0] &&
                        vertices[s][1] == vertices[v3][1] &&
                        vertices[s][2] == vertices[v3][2]) {
                        
                        verticesNormals[s].push_back(tempArray);
                    }
                }
            } else {
                verticesNormals[v1].push_back(tempArray);
                verticesNormals[v2].push_back(tempArray);
                verticesNormals[v3].push_back(tempArray);
            }
            
        }
        
    }
    
    for (int i=0; i<vertices.size(); i++) {
        
        float nx = 0;
        float ny = 0;
        float nz = 0;
        
        for (int j = 0; j < verticesNormals[i].size(); j++) {
            nx += verticesNormals[i][j][0];
            ny += verticesNormals[i][j][1];
            nz += verticesNormals[i][j][2];
        }
        
        if (abs(nx)+abs(ny)+abs(nz) != 0) {
            float div = sqrt(nx*nx+ny*ny+nz*nz);
            nx /= div;
            ny /= div;
            nz /= div;
        } else {
            printf("Something wrong!\n");
        }
        
        array<float, 3> tempArray;
        tempArray[0] = nx;
        tempArray[1] = ny;
        tempArray[2] = nz;
        this->verticesNormals.push_back(tempArray);
        
    }

}

void SceneObject::computeVertices() {
    mat matrix(4,4);
    mat rotationMatrix(4,4);
    
    mat xRotationMatrix(4,4);
    for (int i=0; i<4; i++) {
        for (int j=0; j<4; j++) {
            xRotationMatrix(i,j) = 0;
        }
    }
    xRotationMatrix(0,0) = 1;
    xRotationMatrix(1,1) = 1;
    xRotationMatrix(2,2) = 1;
    xRotationMatrix(3,3) = 1;
    xRotationMatrix(1,1) = cos(rotation[0]*M_PI/180.0);
    xRotationMatrix(2,2) = cos(rotation[0]*M_PI/180.0);
    xRotationMatrix(1,2) = -sin(rotation[0]*M_PI/180.0);
    xRotationMatrix(2,1) = sin(rotation[0]*M_PI/180.0);
    
    mat yRotationMatrix(4,4);
    for (int i=0; i<4; i++) {
        for (int j=0; j<4; j++) {
            yRotationMatrix(i,j) = 0;
        }
    }
    yRotationMatrix(0,0) = 1;
    yRotationMatrix(1,1) = 1;
    yRotationMatrix(2,2) = 1;
    yRotationMatrix(3,3) = 1;
    yRotationMatrix(0,0) = cos(rotation[1]*M_PI/180.0);
    yRotationMatrix(2,2) = cos(rotation[1]*M_PI/180.0);
    yRotationMatrix(0,2) = sin(rotation[1]*M_PI/180.0);
    yRotationMatrix(2,0) = -sin(rotation[1]*M_PI/180.0);
    
    mat zRotationMatrix(4,4);
    for (int i=0; i<4; i++) {
        for (int j=0; j<4; j++) {
            zRotationMatrix(i,j) = 0;
        }
    }
    zRotationMatrix(0,0) = 1;
    zRotationMatrix(1,1) = 1;
    zRotationMatrix(2,2) = 1;
    zRotationMatrix(3,3) = 1;
    zRotationMatrix(0,0) = cos(rotation[2]*M_PI/180.0);
    zRotationMatrix(1,1) = cos(rotation[2]*M_PI/180.0);
    zRotationMatrix(0,1) = -sin(rotation[2]*M_PI/180.0);
    zRotationMatrix(1,0) = sin(rotation[2]*M_PI/180.0);
    
    rotationMatrix = xRotationMatrix * yRotationMatrix * zRotationMatrix;
    
    for (int i=0; i<4; i++) {
        for (int j=0; j<4; j++) {
            matrix(i,j) = 0;
        }
    }
    matrix(0,0) = scale[0];
    matrix(1,1) = scale[1];
    matrix(2,2) = scale[2];
    matrix(3,3) = 1;
    
    matrix(0,3) = position[0];
    matrix(1,3) = position[1];
    matrix(2,3) = position[2];
    
    matrix = matrix * rotationMatrix;
    if(parentMatrix.size() != 0) {
        matrix = parentMatrix*matrix;
    }

    for (int i=0; i<vertices.size(); i++) {
        mat temp(4,1);
        temp(0,0) = vertices[i][0];
        temp(1,0) = vertices[i][1];
        temp(2,0) = vertices[i][2];
        temp(3,0) = 1;
        
        temp = matrix * temp;
        vertices[i][0] = temp(0,0)/temp(3,0);
        vertices[i][1] = temp(1,0)/temp(3,0);
        vertices[i][2] = temp(2,0)/temp(3,0);
    }
    
    for (int i=0; i<children.size(); i++) {
        children[i].setParentMatrix(matrix);
    }
}

void SceneObject::setParentMatrix(mat matrix) {
    parentMatrix = matrix;
}

void SceneObject::writeVertices(ofstream *outputFileStream) {
 
    computeVertices();
    computeNormals();

    for (int i=0; i<vertices.size(); i++) {
        
        *outputFileStream <<fixed<< vertices[i][0] << ", " << vertices[i][1] << ", " << vertices[i][2] << ", ";
        *outputFileStream << verticesNormals[i][0] << ", " << verticesNormals[i][1] << ", " << verticesNormals[i][2] << ", ";
        *outputFileStream << color[0] << ", " << color[1] << ", " << color[2];
        if (i != vertices.size()-1) {
            *outputFileStream << ",";
            *outputFileStream << endl;
        }
    }
    if (children.size() > 0) {
        *outputFileStream << "," << endl;
        for(int i=0; i < children.size(); i++) {
            children[i].writeVertices(outputFileStream);
        }
    }
}

int SceneObject::writeIndices(ofstream *outputFileStream, int offset) {
    
    for (int i=0; i<triangles.size(); i++) {
        *outputFileStream << offset + triangles[i][0]-1 << ", " << offset + triangles[i][1]-1 << ", " << offset + triangles[i][2]-1;
        
        if (i != triangles.size()-1) {
            *outputFileStream << ",";
            *outputFileStream << endl;
        }
    }
    offset += (int)vertices.size();
    if (children.size() > 0) {
        *outputFileStream << "," << endl;
        for(int i=0; i < children.size(); i++) {
            offset = children[i].writeIndices(outputFileStream, offset);
        }
    }
    
    return offset;
}


