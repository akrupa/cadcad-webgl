//
//  main.cpp
//  cadcam-webgl
//
//  Created by Adrian Krupa on 11.04.2014.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#include <iostream>
#include <fstream>
#include "rapidjson/document.h"		// rapidjson's DOM-style API
#include "rapidjson/prettywriter.h"	// for stringify JSON
#include "rapidjson/filestream.h"	// wrapper of C stream for prettywriter as output
#include <cstdio>
#include "Scene.h"

using namespace rapidjson;



int main(int argc, const char * argv[])
{
    Scene *scene = new Scene();
    char file[] = "sample.json";
    if(scene -> parseSceneFromFile(file) != 0) {
        return -1;
    }
    
    if(scene -> loadObjects() != 0) {
        return -1;
    }
    
    scene -> generateHTML();
	return 0;
}

