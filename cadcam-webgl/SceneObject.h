//
//  SceneObject.h
//  cadcam-webgl
//
//  Created by Adrian Krupa on 12.04.2014.
//  Copyright (c) 2014 AKBC. All rights reserved.
//

#ifndef __cadcam_webgl__SceneObject__
#define __cadcam_webgl__SceneObject__

#include <iostream>
#include <vector>
#include <array>
#include <string>
#include "rapidjson/document.h"
#include "armadillo"

using namespace arma;
using namespace rapidjson;
using namespace std;

class SceneObject {
    
    float position[3];
    float rotation[3];
    float scale[3];
    float color[3];
    string objectFilename;
    string objectName;
    mat parentMatrix;
    vector<SceneObject> children;
    vector<array<float, 3>> vertices;
    vector<array<float, 3>> verticesNormals;
    vector<array<int, 3>> triangles;
    
    
    int parseName(Value& obj);
    int parseObjectName(Value& obj);
    int parsePosition(Value& obj);
    int parseRotation(Value& obj);
    int parseScale(Value& obj);
    int parseColor(Value& obj);
    int parseChildren(Value& obj);

    
public:
    SceneObject();
    int parseObject(Value& obj);
    int loadObject();
    void computeNormals();
    void computeVertices();
    void writeVertices(ofstream *outputFileStream);
    int writeIndices(ofstream *outputFileStream, int offset);
    void setParentMatrix(mat matrix);
};

#endif /* defined(__cadcam_webgl__SceneObject__) */
